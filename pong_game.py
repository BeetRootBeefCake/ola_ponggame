import turtle

#creating window
window = turtle.Screen()
window.title("Pong game")
window.bgcolor("black")
window.setup(width=800, height=600)
window.tracer(0)

#points
points_one = 0
point_two = 0

#creating Items
#Paddle one
paddle_one = turtle.Turtle()
paddle_one.speed(0)
paddle_one.shape("square")
paddle_one.color("white")
paddle_one.shapesize(stretch_wid=5, stretch_len=1)
paddle_one.penup()
paddle_one.goto(-350, 0)

#paddle two
paddle_two = turtle.Turtle()
paddle_two.speed(0)
paddle_two.shape("square")
paddle_two.color("white")
paddle_two.shapesize(stretch_wid=5, stretch_len=1)
paddle_two.penup()
paddle_two.goto(350, 0)

#ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("white")
ball.penup()
ball.goto(0, 0)
ball.dx = 0.2
ball.dy = -0.2

#scoring
score_count = turtle.Turtle()
score_count.speed()
score_count.color("white")
score_count.penup()
score_count.hideturtle()
score_count.goto(0, 260)
score_count.write("Player One: 0 Player Two:0", align ="center", font =("Courier", 24, "normal"))

#Functions to move paddles
def paddle_one_up():
    y = paddle_one.ycor()
    y +=20
    paddle_one.sety(y)

def paddle_one_down():
    y = paddle_one.ycor()
    y -=20
    paddle_one.sety(y)

def paddle_two_up():
    y = paddle_two.ycor()
    y +=20
    paddle_two.sety(y)

def paddle_two_down():
    y = paddle_two.ycor()
    y -=20
    paddle_two.sety(y)

#keyboard binding
window.listen()
window.onkeypress(paddle_one_up, "w")
window.onkeypress(paddle_one_down, "s")
window.onkeypress(paddle_two_up, "Up")
window.onkeypress(paddle_two_down, "Down")





#main game loop
while True:
    window.update()

#ball movement
    ball.setx(ball.xcor()+ball.dx)
    ball.sety(ball.ycor()+ball.dy)
#border checking for the ball
#up and down broader
    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy*=-1

    if ball.ycor() < -290:
        ball.sety(-290)
        ball.dy*=-1
#left and right broader
    if ball.xcor() > 390:
        ball.goto(0, 0)
        ball.dx *= -1
        points_one +=1
        score_count.clear()
        score_count.write("Player One: {} Player Two: {}".format(points_one, point_two), align ="center", font =("Courier", 24, "normal"))
    if ball.xcor() < -390:
        ball.goto(0, 0)
        ball.dx *= -1
        point_two +=1
        score_count.clear()
        score_count.write("Player One: {} Player Two: {}".format(points_one, point_two), align ="center", font =("Courier", 24, "normal"))

#paddle and ball bounce
    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_two.ycor() + 40 and ball.ycor() > paddle_two.ycor() - 40 ):
        ball.setx(340)
        ball.dx *=-1

    if (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_one.ycor() + 40 and ball.ycor() > paddle_one.ycor() - 40 ):
        ball.setx(-340)
        ball.dx *=-1
